#include <iostream>
#include <deque>
#include <functional>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <random>
#include <atomic>
#include <chrono>
#include <iostream>
#include <functional>
#include <algorithm>

#define TIMING

#ifdef TIMING
#define INIT_TIMER auto start=std::chrono::high_resolution_clock::now();
#define START_TIMER start=std::chrono::high_resolution_clock::now();
#define STOP_TIMER(name) std::cout << "RUNTIME of " << name << ": " << \
		std::chrono::duration_cast<std::chrono::milliseconds>( \
					std::chrono::high_resolution_clock::now()-start \
					).count() << "ms \n";
#else
#define INIT_TIMER
#define START_TIMER
#define STOP_TIMER(name)
#endif

class ThreadPool
{
	public:

		/*!
			ThreadPool Constructor: 
						n = number of threads in the threadpool
		*/	
		ThreadPool(unsigned int n=std::thread::hardware_concurrency());
		
		template<class F>	void enqueue(F&& f);
		void waitFinished();
		~ThreadPool();

		unsigned int getProcessed() const {return processed;}

	private:
		std::vector<std::thread> workers;
		std::deque<std::function<void()>> tasks;
		std::mutex queue_mutex;
		std::condition_variable cv_task;
		std::condition_variable cv_finished;
		std::atomic_uint processed;
		unsigned int busy;
		bool stop;

		void thread_proc();	
};

ThreadPool::ThreadPool(unsigned int n): busy(0), processed(0), stop(false)
{
	for(unsigned int i=0;i<n;++i)
	{
		workers.emplace_back(std::bind(&ThreadPool::thread_proc,this));
	}
};

ThreadPool::~ThreadPool()
{
	//	set stop-condition
	std::unique_lock<std::mutex> latch(queue_mutex);
	stop=true;
	cv_task.notify_all();
	latch.unlock();

	for(auto& t:workers)
	{
		t.join();
	}

};

void ThreadPool::thread_proc()
{
	while(true)
	{
		std::unique_lock<std::mutex> latch(queue_mutex);
		cv_task.wait(latch,[this](){return stop || !tasks.empty();});
		if(!tasks.empty())
		{
			//	got work. set busy.
			++busy;
			
			//	pull from queue
			auto fn=tasks.front();
			tasks.pop_front();

			//	release lock. run async
			latch.unlock();
			
			fn();
			++processed;
			
			latch.lock();
			--busy;

			cv_finished.notify_one();
		}
		else if(stop)
		{
			break;
		}
	};

};


/*!
	Generic Function Push
 */
template<class F>
void ThreadPool::enqueue(F&& f)
{
	std::unique_lock<std::mutex> lock(queue_mutex);
	tasks.emplace_back(std::forward<F>(f));
	cv_task.notify_one();
};


/*!
	Waits until the queue is empty
 */
void ThreadPool::waitFinished()
{
	std::unique_lock<std::mutex> lock(queue_mutex);
	cv_finished.wait(lock,[this](){return tasks.empty() && (busy==0);});
};

void work_proc_sort_random_numbers()
{
	std::random_device rd;
	std::mt19937 rng(rd());

	/*!
	 *	Build a vector of random numbers
	 */
	std::vector<int> data;
	data.reserve(100000);
	std::generate_n(std::back_inserter(data),data.capacity(),[&](){ return rng();});
	std::sort(data.begin(),data.end(),std::greater<int>());
};


void test1()
{
	ThreadPool tp(std::thread::hardware_concurrency());

	/*!
	 *	Run five batches of 100 items
	 *  
	 */
	for(int x=0;x<5;++x)
	{
		for(int i=0;i<100;++i)
			tp.enqueue(work_proc_sort_random_numbers);

		tp.waitFinished();
		std::cout << tp.getProcessed() << "\n";
	}
}


void test2()
{
	ThreadPool tp(4);

	/*!
	 *	Run five batches of 100 items
	 *  
	 */
	for(int x=0;x<5;++x)
	{
		for(int i=0;i<100;++i)
			tp.enqueue(work_proc_sort_random_numbers);

		tp.waitFinished();
		std::cout << tp.getProcessed() << "\n";
	}
}

void test3()
{
	ThreadPool tp(2);

	/*!
	 *	Run five batches of 100 items
	 *  
	 */
	for(int x=0;x<5;++x)
	{
		for(int i=0;i<100;++i)
			tp.enqueue(work_proc_sort_random_numbers);

		tp.waitFinished();
		std::cout << tp.getProcessed() << "\n";
	}
}

void test4()
{
	ThreadPool tp(1);

	/*!
	 *	Run five batches of 100 items
	 *  
	 */
	for(int x=0;x<5;++x)
	{
		for(int i=0;i<100;++i)
			tp.enqueue(work_proc_sort_random_numbers);

		tp.waitFinished();
		std::cout << tp.getProcessed() << "\n";
	}
}

int main()
{

	std::cout << "Hardware Concurrency: " << std::thread::hardware_concurrency() << "\n";
	INIT_TIMER

	START_TIMER
	test1();
	STOP_TIMER("Hardware Concurrency");

	START_TIMER
	test2();
	STOP_TIMER("4 threads");

	START_TIMER
	test3();
	STOP_TIMER("2 threads");

	START_TIMER
	test4();
	STOP_TIMER("1 thread");

	return EXIT_SUCCESS;
}
