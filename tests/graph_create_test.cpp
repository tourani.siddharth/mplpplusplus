#include "graph.hxx"
#include "defines.hxx"
#include "graph_create.hxx"
#include "handshake_algo_MT.hxx"
#include "simd_vectors.hxx"

int main()
{
    Graph<float> gr=create_grid_graph<float>(4,4,4,true);
//    Graph<float> gr=create_complete_graph<float>(5,4,true);
    gr.compute_neighbor_nodes();
    gr.print();
    handshake_algo_MT<sse_float_4>::options opts;
    handshake_algo_MT<sse_float_4> algo(gr,opts);
    algo.print_aligned_allocator();
    algo.compute_ordering_boost();
    std::vector<int> ordering=algo.m_ordering;
    std::cout << "ordering: ";
    for(int i=0;i<ordering.size();++i)
    {
        std::cout << ordering[i] << " ";
    }
    std::cout << "\n";
	return 0;
}
