#include <string>
#include <deque>
#include <iostream>
#include <atomic>
#include <thread>
#include <random>
#include <condition_variable>
#include <mutex>
#include <functional>
#include <algorithm>
//#include <LoggerCpp/LoggerCpp.h>
#include "Logger.h"


using namespace std;

// The function we want to execute on the new thread.
void task1(string msg)
{
    cout << "task1 says: " << msg;
}


class thread_pool
{
	public:
		thread_pool(unsigned int n=std::thread::hardware_concurrency());
		void waitFinished();
		template<class F> void enqueue(F&& f);
		void thread_proc(const int idx);
		~thread_pool();
		void wait_finished();
		int get_processed();
	
		std::vector<std::thread> workers;				//	
		std::mutex queue_mutex;							//	
		std::condition_variable cv_task;				//	
		std::condition_variable cv_finished;			//	
		std::atomic<int> processed;			//	
		std::atomic_int busy;							//	
		std::atomic<bool> stop;							//	
		std::deque<std::function<void()> > tasks;		//	
		logger m_logger;								//	
};	


/*
 *	 
 */	
thread_pool::thread_pool(unsigned int n): m_logger("main.tester")
{
	for(int i=0;i<n;++i)
	{
		workers.emplace_back(std::bind(&thread_pool::thread_proc,this));		//	launch the threads
	}
}

thread_pool::~thread_pool()
{
	/*
	 *	mutex protects stop and 
	 *	
	 */
	std::unique_lock<std::mutex> latch_lock(queue_mutex);
	stop=true;																	//	set stop to true
	cv_task.notify_all();														//	notify all the 
	latch_lock.unlock();														//	unlock the latch
	
	for(int i=0;i<workers.size();++i)
		workers[i].join();
}

int thread_pool::get_processed()
{
	return processed;
}

void thread_pool::thread_proc(const int idx)
{
	while (true)																//	by default the thread loops
    {
		//std::string str="thread " + std::to_string(idx);						//	
		//m_logger() << str;														//	That is a factual claim. 
		std::unique_lock<std::mutex> latch(queue_mutex);						//	it acquires the lock for the queue mutex
		cv_task.wait(latch, [this](){ return !tasks.empty() || stop; });      	//  then waits if either stop is true or there are tasks in the queue to process

		if(stop)																//	EXIT CONDITION
		{
			latch.unlock();														//	
			break;																//	get out of the while loop
		}
			
		++busy;																	//	
		auto fn=tasks.front();													//	
		tasks.pop_front();														//	
		latch.unlock();															//	
		
		fn();																	//	
		++processed;															//	

		latch.lock();															//	
		--busy;																	//	
		latch.unlock();															//	

		cv_task.notify_one();													//	notify one task
    }
}

//	enqueue
template<class F>
void thread_pool::enqueue(F&& f)
{
    std::unique_lock<std::mutex> lock(queue_mutex);             //  
    tasks.emplace_back(std::forward<F>(f));                     //  put one element at the end of the queue
    cv_task.notify_one();                                       //  notify one thread
}	

// waits until the queue is empty.
void thread_pool::wait_finished()
{
    std::unique_lock<std::mutex> lock(queue_mutex);
    cv_finished.wait(lock, [this](){ return tasks.empty() && (busy == 0); });                   //  lock the queue and wait until tasks is empty and busy==0
    lock.unlock();
}

void work_proc()
{
	std::random_device rd;
    std::mt19937 rng(rd());

    // build a vector of random numbers
    std::vector<int> data;
    data.reserve(100000);
    std::generate_n(std::back_inserter(data), data.capacity(), [&](){ return rng(); });
    std::sort(data.begin(), data.end(), std::greater<int>());
}

int main()
{
    thread_pool tp;

    // run five batches of 100 items
    //for (int x=0; x<5; ++x)
    //{
        // queue 100 work tasks
        for (int i=0; i<500; ++i)
            tp.enqueue(work_proc);

        tp.wait_finished();
        std::cout << tp.get_processed() << '\n';
    //}
    
    return EXIT_SUCCESS;
}
