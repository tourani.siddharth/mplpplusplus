#include <xmmintrin.h>  //  SSE
#include <emmintrin.h>  //  SSE2
#include <immintrin.h>  //  avx

#include <iostream>
#include <vector>
#include <cstring>

#include "defines.hxx"

#include "simple_timer.hxx"

template<typename T>
void print_vector(const float* vec, int len_of_vec)
{
    for(int i=0;i<len_of_vec;++i)   
        std::cout << vec[i] << " ";
    std::cout << "\n";
}

void fill_up_vector(const int sz1, float** ptr)
{
    *ptr=(float*)malloc(sizeof(float)*sz1);
    for(int i=0;i<sz1;++i)
    {
        (*ptr)[i]=static_cast<float>(rand()%10+1);
    }
}

void fill_up_table(const int sz1, const int sz2, const int skip_step, float** ptr)
{
    *ptr=(float*)malloc(sizeof(float)*sz1*skip_step);

    for(int i=0;i<sz1*skip_step;++i)    (*ptr)[i]=0;

    for(int i=0;i<sz1;++i)
    {
        for(int j=0;j<sz2;++j)
        {
            (*ptr)[i*skip_step+j]=static_cast<float>(rand()%10+1);
        }
    } 
}

void print_table(float** ptr, const int sz1, const int sz2, const int skip_step)
{
    for(int i=0;i<sz1;++i)
    {
        for(int j=0;j<sz2;++j)
        {
            std::cout << (*ptr)[i*skip_step+j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

void print_table_full(float** ptr, const int sz1, const int sz2, const int skip_step)
{
    for(int i=0;i<sz1;++i)
    {
        for(int j=0;j<skip_step;++j)
        {
            std::cout << (*ptr)[i*skip_step+j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

void do_handshake(float**un1, float** pw, float** un2, const int sz1, const int sz2, const int new_sz2, const int skip_step)
{
    for(int i=0;i<sz1;++i)
    {
        for(int j=0;j<sz2;++j)
        {
           (*pw)[i*new_sz2+j]+=(*un2)[j]+(*un1)[i];
        }
    }

    for(int i=0;i<sz1;++i)
    {
        float min_val=(*pw)[i*new_sz2+0];
        for(int j=1;j<sz2;++j)
        {
            if(min_val>(*pw)[i*new_sz2+j])
            {
                min_val=(*pw)[i*new_sz2+j];
            }
        }
        
        min_val*=0.5;
        for(int j=0;j<sz2;++j)
        {
            (*pw)[i*new_sz2+j]-=min_val;
        }
        (*un1)[i]=min_val;
    }
 
    for(int j=0;j<sz2;++j)
    {
        float min_val=(*pw)[0*new_sz2+j];
        for(int i=1;i<sz1;++i)
        {
            if(min_val>(*pw)[i*new_sz2+j])
            {
                min_val=(*pw)[i*new_sz2+j];
            }
        }
        for(int i=0;i<sz1;++i)
        {
            (*pw)[i*new_sz2+j]-=min_val;
        }
        (*un2)[j]=min_val;
    }

    for(int i=0;i<sz1;++i)
    {
        float min_val=(*pw)[i*new_sz2+0];
        for(int j=1;j<sz2;++j)
        {
            if(min_val>(*pw)[i*new_sz2+j])
            {
                min_val=(*pw)[i*new_sz2+j];
            }
        }
        
        for(int j=0;j<sz2;++j)
        {
            (*pw)[i*new_sz2+j]-=min_val;
        }
        (*un1)[i]+=min_val;
    }
}

template<typename T>
void do_handshake_simd2(T** un1, T** pw, T** un2, const int sz1, const int sz2, const int new_sz2, const int skip_step)
{
    __m128 un2_vec,pw_vec,un1_vec,min_vec,w;
    T* vv=(T*)malloc(sizeof(T)*4);

    T* un1_ptr=(*un1);
    T* un2_ptr=(*un2);
    T* pw_ptr=*pw; 
    float min_val=std::numeric_limits<T>::infinity();

    int j,i=0;
    int j_steps=sz2/skip_step;

    //  FIRST PULL
    for(int i=0;i<sz1;++i)
    {
        min_vec=_mm_set1_ps(std::numeric_limits<T>::infinity());       
        un1_vec=_mm_set1_ps(un1_ptr[i]);
        for(j=0;j<j_steps;++j)
        {
            un2_vec=_mm_load_ps(&un2_ptr[j*skip_step]);
            pw_vec=_mm_load_ps(&pw_ptr[i*new_sz2+j*skip_step]);
            pw_vec=_mm_add_ps(_mm_add_ps(un2_vec,pw_vec),un1_vec);
            min_vec=_mm_min_ps(min_vec,pw_vec);                     //  min_vec will contain the four lowest elements
            _mm_store_ps(&pw_ptr[i*new_sz2+j*skip_step],pw_vec);
        }
        
        w = _mm_shuffle_ps(min_vec, min_vec, _MM_SHUFFLE(2, 3, 0, 1));
        w = _mm_min_ps(min_vec, w); // min(0,2) | min(1,3) | min(2,0) | min(3,1)
        min_vec = _mm_shuffle_ps(w, w, _MM_SHUFFLE(1, 0, 3, 2));
        min_vec = _mm_min_ps(min_vec, w); // min in all
        _mm_store_ps(vv,min_vec);

        min_val=vv[0];

        for(j=j_steps*skip_step;j<sz2;++j)  
        {
            pw_ptr[i*new_sz2+j]+=un2_ptr[j]+un1_ptr[i];
            if(pw_ptr[i*new_sz2+j]<min_val)
            {
                min_val=pw_ptr[i*new_sz2+j];
            }
        }

        min_val*=0.5;
        un1_ptr[i]=min_val;

        for(j=0;j<j_steps;++j)
        {
            pw_vec=_mm_load_ps(&pw_ptr[i*new_sz2+j*skip_step]);
            pw_vec=_mm_sub_ps(pw_vec,_mm_set1_ps(min_val));
            _mm_store_ps(&pw_ptr[i*new_sz2+j*skip_step],pw_vec);
        }
        for(j=j_steps*skip_step;j<sz2;++j)
        {
            pw_ptr[i*new_sz2+j]-=min_val;
        }
    } 

    //  SECOND PULL
    for(int j=0;j<sz2;++j)
    {
        T min_val=(*pw)[0*new_sz2+j];
        for(int i=1;i<sz1;++i)
        {
            if(min_val>(*pw)[i*new_sz2+j])
            {
                min_val=(*pw)[i*new_sz2+j];
            }
        }
        for(int i=0;i<sz1;++i)
        {
            (*pw)[i*new_sz2+j]-=min_val;
        }
        (*un2)[j]=min_val;
    }

    //  THIRD PULL
    for(int i=0;i<sz1;++i)
    {
        min_vec=_mm_set1_ps(std::numeric_limits<T>::infinity());    //   
        for(j=0;j<j_steps;++j)
        {
            pw_vec=_mm_load_ps(&pw_ptr[i*new_sz2+j*skip_step]);         //  
            min_vec=_mm_min_ps(min_vec,pw_vec);                         //  min_vec will contain the four lowest elements
            _mm_store_ps(&pw_ptr[i*new_sz2+j*skip_step],pw_vec);        //  
        }
        
        w = _mm_shuffle_ps(min_vec, min_vec, _MM_SHUFFLE(2, 3, 0, 1));
        w = _mm_min_ps(min_vec, w); // min(0,2) | min(1,3) | min(2,0) | min(3,1)
        min_vec = _mm_shuffle_ps(w, w, _MM_SHUFFLE(1, 0, 3, 2));
        min_vec = _mm_min_ps(min_vec, w); // min in all
        _mm_store_ps(vv,min_vec);

        min_val=vv[0];

        for(j=j_steps*skip_step;j<sz2;++j)  
        {
            if(pw_ptr[i*new_sz2+j]<min_val)
            {
                min_val=pw_ptr[i*new_sz2+j];
            }
        }

        un1_ptr[i]+=min_val;
        
        for(j=0;j<j_steps;++j)
        {
            pw_vec=_mm_load_ps(&pw_ptr[i*new_sz2+j*skip_step]);
            pw_vec=_mm_sub_ps(pw_vec,_mm_set1_ps(min_val));
            _mm_store_ps(&pw_ptr[i*new_sz2+j*skip_step],pw_vec);
        }
        for(j=j_steps*skip_step;j<sz2;++j)
        {
            pw_ptr[i*new_sz2+j]-=min_val;
        }
    } 
}

template<typename T>
void do_handshake_simd(T** un1, T** pw, T** un2, const int sz1, const int sz2, const int new_sz2, const int skip_step)
{
    __m128 un2_vec,pw_vec,un1_vec,min_vec,w;
    T* vv=(T*)malloc(sizeof(T)*4);

    T* un1_ptr=(*un1);
    T* un2_ptr=(*un2);
    T* pw_ptr=*pw; 
    T min_val=std::numeric_limits<T>::infinity();

    int j,i=0;
    int j_steps=sz2/skip_step;

    //  FIRST PULL
    for(int i=0;i<sz1;++i)
    {
        min_vec=_mm_set1_ps(std::numeric_limits<T>::infinity());       
        un1_vec=_mm_set1_ps(un1_ptr[i]);
        for(j=0;j<j_steps;++j)
        {
            un2_vec=_mm_load_ps(&un2_ptr[j*skip_step]);
            pw_vec=_mm_load_ps(&pw_ptr[i*new_sz2+j*skip_step]);
            pw_vec=_mm_add_ps(_mm_add_ps(un2_vec,pw_vec),un1_vec);
            min_vec=_mm_min_ps(min_vec,pw_vec);                     //  min_vec will contain the four lowest elements
            _mm_store_ps(&pw_ptr[i*new_sz2+j*skip_step],pw_vec);
        }
        
        w = _mm_shuffle_ps(min_vec, min_vec, _MM_SHUFFLE(2, 3, 0, 1));
        w = _mm_min_ps(min_vec, w); // min(0,2) | min(1,3) | min(2,0) | min(3,1)
        min_vec = _mm_shuffle_ps(w, w, _MM_SHUFFLE(1, 0, 3, 2));
        min_vec = _mm_min_ps(min_vec, w); // min in all
        _mm_store_ps(vv,min_vec);

        min_val=vv[0];

        for(j=j_steps*skip_step;j<sz2;++j)  
        {
            pw_ptr[i*new_sz2+j]+=un2_ptr[j]+un1_ptr[i];
            if(pw_ptr[i*new_sz2+j]<min_val)
            {
                min_val=pw_ptr[i*new_sz2+j];
            }
        }

        min_val*=0.5;
        un1_ptr[i]=min_val;

        for(j=0;j<j_steps;++j)
        {
            pw_vec=_mm_load_ps(&pw_ptr[i*new_sz2+j*skip_step]);
            pw_vec=_mm_sub_ps(pw_vec,_mm_set1_ps(min_val));
            _mm_store_ps(&pw_ptr[i*new_sz2+j*skip_step],pw_vec);
        }
        for(j=j_steps*skip_step;j<sz2;++j)
        {
            pw_ptr[i*new_sz2+j]-=min_val;
        }
    }


    //  SECOND PULL
    for(j=0;j<j_steps;++j)
    {
        //  compute the min value for 4 columns
        min_vec=_mm_load_ps(&pw_ptr[0*new_sz2+j*skip_step]);
        for(int i=1;i<sz1;++i)
        {
            min_vec=_mm_min_ps(_mm_load_ps(&pw_ptr[i*new_sz2+j*skip_step]),min_vec);
        }

        _mm_store_ps(vv,min_vec);
        
        for(int k=j*skip_step;k<j*skip_step+4;++k)
        {
            un2_ptr[k]=vv[k];
        }
        
        for(int i=0;i<sz1;++i)
        {
            pw_vec=_mm_load_ps(&pw_ptr[i*new_sz2+j*skip_step]);
            pw_vec=_mm_sub_ps(pw_vec,min_vec);
            _mm_store_ps(&pw_ptr[i*new_sz2+j*skip_step],pw_vec);
        }
    }
    
    for(int j=j_steps*skip_step;j<sz2;++j)
    {
        min_val=pw_ptr[0*new_sz2+j];
        for(int i=1;i<sz1;++i)
        {
            min_val=std::min(min_val,pw_ptr[i*new_sz2+j]);
        }
        un2_ptr[j]=min_val;
        for(int i=0;i<sz1;++i)
        {
            pw_ptr[i*new_sz2+j]-=min_val;
        }
    }
    
    for(int i=0;i<sz1;++i)
    {
        min_vec=_mm_set1_ps(std::numeric_limits<T>::infinity());    //   
        for(j=0;j<j_steps;++j)
        {
            pw_vec=_mm_load_ps(&pw_ptr[i*new_sz2+j*skip_step]);         //  
            min_vec=_mm_min_ps(min_vec,pw_vec);                         //  min_vec will contain the four lowest elements
            _mm_store_ps(&pw_ptr[i*new_sz2+j*skip_step],pw_vec);        //  
        }
        
        w = _mm_shuffle_ps(min_vec, min_vec, _MM_SHUFFLE(2, 3, 0, 1));
        w = _mm_min_ps(min_vec, w); // min(0,2) | min(1,3) | min(2,0) | min(3,1)
        min_vec = _mm_shuffle_ps(w, w, _MM_SHUFFLE(1, 0, 3, 2));
        min_vec = _mm_min_ps(min_vec, w); // min in all
        _mm_store_ps(vv,min_vec);

        min_val=vv[0];

        for(j=j_steps*skip_step;j<sz2;++j)  
        {
            if(pw_ptr[i*new_sz2+j]<min_val)
            {
                min_val=pw_ptr[i*new_sz2+j];
            }
        }

        un1_ptr[i]+=min_val;
        
        for(j=0;j<j_steps;++j)
        {
            pw_vec=_mm_load_ps(&pw_ptr[i*new_sz2+j*skip_step]);
            pw_vec=_mm_sub_ps(pw_vec,_mm_set1_ps(min_val));
            _mm_store_ps(&pw_ptr[i*new_sz2+j*skip_step],pw_vec);
        }
        for(j=j_steps*skip_step;j<sz2;++j)
        {
            pw_ptr[i*new_sz2+j]-=min_val;
        }
    } 
}

int main()
{
    int sz1=430;
    int sz2=731;
    int size_of_vector=4;

    INIT_TIMER

    int new_sz2=RND_UP(sz2,size_of_vector);
    std::cout << "new_sz2: " << new_sz2 << "\n";

    float* pw;
    fill_up_table(sz1,sz2,new_sz2,&pw);
    std::cout << "\nPW\n";

    float* pw2=(float*)malloc(sizeof(float)*sz1*new_sz2);
    for(int ii=0;ii<sz1*new_sz2;++ii)   pw2[ii]=pw[ii];

    float* pw3=(float*)malloc(sizeof(float)*sz1*new_sz2);
    for(int ii=0;ii<sz1*new_sz2;++ii)   pw3[ii]=pw[ii];

    float* un1;
    fill_up_vector(sz1,&un1);

    float* un1_2=(float*)malloc(sizeof(float)*sz1);
    for(int ii=0;ii<sz1;++ii)   un1_2[ii]=un1[ii];

    float* un1_3=(float*)malloc(sizeof(float)*sz1);
    for(int ii=0;ii<sz1;++ii)   un1_3[ii]=un1[ii];

    float* un2;
    fill_up_vector(sz2,&un2);

    float* un2_2=(float*)malloc(sizeof(float)*sz2);
    for(int ii=0;ii<sz2;++ii)   un2_2[ii]=un2[ii];

    float* un2_3=(float*)malloc(sizeof(float)*sz2);
    for(int ii=0;ii<sz2;++ii)   un2_3[ii]=un2[ii];

    int numTimes=1000;

    START_TIMER
    for(int i=0;i<numTimes;++i)
    {
        do_handshake(&un1,&pw,&un2,sz1,sz2,new_sz2,size_of_vector);
    }
    STOP_TIMER("do_handshake")
        
    START_TIMER
    for(int i=0;i<numTimes;++i)
    {
        do_handshake_simd<float>(&un1_2,&pw2,&un2_2,sz1,sz2,new_sz2,size_of_vector);
    }
    STOP_TIMER("do_handshake_simd")
        
    START_TIMER
    for(int i=0;i<numTimes;++i)
    {
        do_handshake_simd2(&un1_3,&pw3,&un2_3,sz1,sz2,new_sz2,size_of_vector);
    }
    STOP_TIMER("do_handshake_simd2")

    return EXIT_SUCCESS;
}
