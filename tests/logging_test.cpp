
#include "handshake_algo_MT.hxx"
#include "graph.hxx"
#include "defines.hxx"
#include "graph_create.h"
#include "handshake_algo_MT.hxx"
#include "simd_vectors.hxx"


/*
 *		HOW TO USE TIMERS:  
 * 			1)     INIT_TIMER
 * 			2) 		START_TIMER
 * 			3)		STOP_TIMER(objectTimed)
 * 
 * 
 * 
 * 		HOW TO USE logtk:
 * 			1)    logtk::start_logger("my_log",".");		//	"my_log" is the name of the log file, "." is the location where it will be stored
 * 			2) 	  log("log[%03d] Something...",i);			//	log, follows the printf formula 
 * 					Another possibility, logtk::log("Beginning Locked Handshake [%d] [%d]",edge_idx, thread_idx);
 * 			3) 	  logtk::stop();
 * 
 * 		
 * 	
 */

int main()
{
    Graph<float> gr=create_grid_graph<float>(4,4,4,true);
//    Graph<float> gr=create_complete_graph<float>(5,4,true);
    gr.compute_neighbor_nodes();
    gr.print();
    handshake_algo_MT<sse_float_4>::options opts;
    handshake_algo_MT<sse_float_4> algo(gr,opts);
    algo.print_aligned_allocator();
    std::vector<int> ordering=algo.compute_ordering_boost();		//	Right, now that I have computed the ordering according to boost
    std::cout << "ordering: ";
    for(int i=0;i<ordering.size();++i)
    {
        std::cout << ordering[i] << " ";
    }
    std::cout << "\n";
	return 0;
}
