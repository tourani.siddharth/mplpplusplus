#include "graph_io.hxx"
#include "defines.hxx"
#include "handshake_algo_MT.hxx"
#include "simple_timer.hxx"


int main(const int argc, const char* argv[])
{
    std::string filename=argv[1];
    std::cout << "filename: "  << filename << "\n";

    Graph<float> gr=read_graph<float>(filename);


    handshake_algo_MT<sse_float_4>::options opts;
    opts.num_iters=10;
    opts.num_threads=8;

    handshake_algo_MT<sse_float_4> algo(gr,opts);

    algo.infer();




    return EXIT_SUCCESS;
}
