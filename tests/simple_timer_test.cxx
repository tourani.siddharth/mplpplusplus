#include "simple_timer.hxx"

int main()
{

    INIT_TIMER
    START_TIMER
    sleep(2);
    STOP_TIMER("sleeping for 2 seconds")
    START_TIMER
    long unsigned int b=0;
    for(int i=0;i<10000000;++i)
    {
        b+=i;
    }
    STOP_TIMER("some long loop")

    return EXIT_SUCCESS;
}
