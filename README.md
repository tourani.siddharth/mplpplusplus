#MPLP++: Fast, Parallel Dual Block-Coordinate Ascent for Dense Graphical Models
 
> Dense, discrete Graphical Models with pairwise potentials are a powerful 
class  of  models  which  are  employed  in  state-of-the-art  computer  vision
and bio-imaging applications. This work introduces a new MAP-solver, based
on the popular Dual Block-Coordinate Ascent principle. Surprisingly, by making
a small change to the low-performing solver, the Max Product Linear Programming 
(MPLP) algorithm [1], we derive the new solver MPLP++ that significantly
outperforms all existing solvers by a large margin, including the state-of-the-art
solver Tree-Reweighted Sequential (TRW-S) message-passing algorithm [2]. 
Additionally, our solver is highly parallel, in contrast to TRW-S, which gives 
a further boost in performance with the proposed GPU and multi-thread CPU
implementations. We verify the superiority of our algorithm on dense problems from
publicly available benchmarks, as well, as a new benchmark for 6D Object Pose
estimation. We also provide an ablation study with respect to graph density.

If you find this code useful for your research, please cite


```
@INPROCEEDINGS{MPLPPPBCA2018ECCV,
  author = {Siddharth Tourani, Alexander Shekhovtsov, Carsten Rother, Bogdan Savchynskyy},
  title = {MPLP++: Fast, Parallel Dual Block-Coordinate Ascent for Dense Graphical Models},
  booktitle = {European Conference on Computer Vision (ECCV)},
  year = {2018}
} 
```

## Code Overview
- `include` - Contains the main Code
- `LoggerCpp` - g Library.
- `test/` - Folder of unit tests
- `example/example1.cxx` - Example Code for how to use algorithm.			

## Graph Format
Graph txt file has the following format:

```
nV nF   %   nV=(number of vertices), nF=(number of factors)
```
Following these are nF information blocks. They are of two types: unary and pairwise.

Unary blocks have the following format:
```
1	        % indicates the block is a unary block
node_id L   % (id of the vertex) and  (number of labels in the node)
X1 ... XL   % values in the vertex
```

Pairwise blocks have the following format:
```
2                               %    indicates the block is a pairwise block
node1_id node2_id M N           %   (id of the 1st vertex), (id of the 2nd vertex), (number of labels in the 1st vertex), (number of labels in the 2nd vertex)
M x N floats                    %   which makeup the pairwise term
```

