#pragma once


#include "node.hxx"
#include "edge.hxx"
#include "aligned_allocator.hxx"
#include "utils.hxx"
#include "defines.hxx"

template<typename T>
class dGraph_data_storage
{
public:

    std::vector<std::vector<T>> m_unaries;
    std::vector<std::vector<T>> m_pairwise;
};

template<typename T>
class dGraph
{
public:

    dGraph():m_nV(0),m_nE(0)
    {

    }

    dGraph(const dGraph<T>& graph)
    {
        std::cout << "SLOW COPY CONSTRUCTOR\n";
        for(int i=0;i<graph.m_nodes.size();++i)
        {
            m_nodes.push_back(graph.m_nodes[i]);
        }

        for(int i=0;i<graph.m_edges.size();++i)
        {
            m_edges.push_back(graph.m_edges[i]);
        }

        m_edge_indices=graph.m_edge_indices;
        m_nV=graph.m_nV;
        m_nE=graph.m_nE;

        m_data_storage.m_unaries=graph.m_data_storage.m_unaries;
        m_data_storage.m_pairwise=graph.m_data_storage.m_pairwise;
    }

    //  AddNode to the graph
    void AddNode(const dNode<T>& node, const std::vector<T>& vec)
    {
        m_nodes.push_back(node);
        m_data_storage.m_unaries.push_back(vec);
        m_nV++;
    }

    //  AddEdge to the graph
    void AddEdge(const Edge<T>& edge, const std::vector<T>& vec)
    {
        m_edges.push_back(edge);                        //
        std::pair<int,int> m(edge.m_from,edge.m_to);    //
        m_edge_indices.push_back(m);                    //
        m_data_storage.m_pairwise.push_back(vec);
        m_nE++;
    }

    //  compute neighbor nodes
    void compute_neighbor_nodes()
    {
        for(auto i=0;i<m_edge_indices.size();++i)
        {
            dNode<T>& nd1=m_nodes[m_edge_indices[i].first];     //
            dNode<T>& nd2=m_nodes[m_edge_indices[i].second];    //
            nd1.m_outgoing_edges.push_back(i);                  //
            nd2.m_incoming_edges.push_back(i);                  //
        }
    }

    void print()
    {

        std::cout << "--------INSIDE HERE----------\n";
        std::cout << "number of nodes: " << m_nodes.size() << "\n";
        std::cout << "number of edges: " << m_edges.size() << "\n";
        std::cout << "number of edge indices: " << m_edge_indices.size() << "\n";

        std::cout << "\nEdge Indices\n";
        for(int i=0;i<m_edge_indices.size();++i)
        {
            std::cout << m_edge_indices[i].first << " -> " << m_edge_indices[i].second << " || ";
        }
        std::cout << "\n\n";

        std::cout << "-------------------- PRINT UNARIES --------------------------\n";
        for(dNode<T>& nd:m_nodes)
        {
            nd.print();
            std::cout << "\n";
        }
        std::cout << "----------------------------- DONE ------------------------------\n\n";

        std::cout << "-------------------- PRINT EDGES --------------------------\n";
        for(auto& ed:m_edges)
        {
            ed.print();
        }
        std::cout << "----------------------------- DONE ------------------------------\n\n";

        std::cout << "-------------------- PRINT UNARY DATA --------------------------\n";
        for(int i=0;i<m_data_storage.m_unaries.size();++i)
        {
            std::string m_un="UNARY_" + std::to_string(m_nodes[i].m_idx) + ": ";
            print_vector(m_data_storage.m_unaries[i],m_un);
        }
        std::cout << "---------------------------- DONE -------------------------------\n\n";

        std::cout << "-------------------- PRINT PAIRWISE DATA --------------------------\n";
        for(int j=0;j<m_data_storage.m_pairwise.size();++j)
        {
            std::string  m_pw="PAIRWISE_" + std::to_string(m_edges[j].m_from)  + " -> " +std::to_string(m_edges[j].m_to) + ": ";
            print_table(m_data_storage.m_pairwise[j],m_edges[j].m_d1,m_edges[j].m_d2,m_pw);
        }
        std::cout << "---------------------------- DONE -------------------------------\n\n";
    }

    inline const int nV()
    {
        return m_nV;
    }

    inline const int nE()
    {
        return m_nE;
    }

    int m_nV;
    int m_nE;
    std::vector<dNode<T>> m_nodes;
    std::vector<Edge<T>> m_edges;
    std::vector<std::pair<int,int>> m_edge_indices;
    dGraph_data_storage<T> m_data_storage;
};
