#pragma once

#include "defines.hxx"

template<typename T>
inline void print_vector(const std::vector<T>& vec)
{
    for(T item:vec)
    {
        std::cout << item << " ";
    }
    std::cout << "\n";
}

template<typename T>
inline void print_vector(const std::vector<T>& vec, const std::string& name)
{
    std::cout << name << ": ";
    for(T item:vec)
    {
        std::cout << item << " ";
    }

    std::cout << "\n";
}

template<typename T>
inline void print_vector(const T* ptr, const int sz)
{
    for(int i=0;i<sz;++i)
    {
        std::cout << *(ptr+i) << " ";
    }
    std::cout << "\n";
}

template<typename T>
inline void print_vector(const T* ptr, const int sz, const std::string& name)
{
    std::cout << name << ": ";
    for(int i=0;i<sz;++i)
    {
        std::cout << *(ptr+i) << " ";
    }
    std::cout << "\n";
}


template<typename T>
inline void print_table(const std::vector<T>& vec, const int sz1, const int sz2)
{
    for(int i=0;i<sz1;++i)
    {
        for(int j=0;j<sz2;++j)
        {
            std::cout << vec[i*sz2+j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

template<typename T>
inline void print_table(const std::vector<T>& vec, const int sz1, const int sz2, const std::string& name)
{
    std::cout << name << "\n";
    for(int i=0;i<sz1;++i)
    {
        for(int j=0;j<sz2;++j)
        {
            std::cout << vec[i*sz2+j] << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

template<typename T>
inline void print_table(const T* ptr, const int sz1 , const int sz2)
{
    for(int i=0;i<sz1;++i)
    {
        for(int j=0;j<sz2;++j)
        {
            std::cout << *(ptr+i*sz2+j) << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

/*
template<typename T>
inline void print_queue(std::queue<T> q, const std::string& name)
{
    std::cout << name << ": ";
    while(!q.empty())
    {
        std::cout << q.front() << " ";
        q.pop();
    }
    std::cout << "\n";
}
*/

template<typename T>
inline void print_table(const T* ptr, const int sz1, const int sz2, const std::string& name)
{
    std::cout << name << ": ";
    for(int i=0;i<sz1;++i)
    {
        for(int j=0;j<sz2;++j)
        {
            std::cout << *(ptr+i*sz2+j) << " ";
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

template<typename T>
inline std::vector<T> create_random_vector(const int len, const int max_val)
{
    std::vector<T> vec(len);
    for(int i=0;i<len;++i)
    {
        vec[i]=rand()%max_val;
    }
    return vec;
}

template<typename T>
inline int compute_min_array_location(T* arr, const int sz)
{
    T min_val=std::numeric_limits<T>::infinity();
    int min_idx=-1;
    for(int i=0;i<sz;++i)
    {
        if(arr[i]<min_val)
        {
            min_val=arr[i];
            min_idx=i;
        }
    }
    return min_idx;
}

template<typename T>
inline int compute_min_array_location(T* arr, const int sz,T* val)
{
    T min_val=std::numeric_limits<T>::infinity();
    int min_idx=-1;
    for(int i=0;i<sz;++i)
    {
        if(arr[i]<min_val)
        {
            min_val=arr[i];
            min_idx=i;
        }
    }
    *val=min_val;
    return min_idx;
}

template<typename T>
inline T compute_min(T* arr, const int sz)
{
    T val=arr[0];
    for(auto i=1;i<sz;++i)
    {
        if(arr[i]<val)
        {
            val=arr[i];
        }
    }
    return val;
}

template<typename T>
inline T compute_min(const std::vector<T>& arr)
{
    assert(arr.size()>0);
    T val=arr[0];
    for(auto i=1;i<arr.size();++i)
    {
        if(arr[i]<val)
        {
            val=arr[i];
        }
    }
    return val;
}

inline void print_dashed_line()
{
    std::cout << "-------------------------------------------------------------------------\n";
}

inline void print_named_dashed_line(const std::string& str)
{
    std::cout << "-----------------------------" << str << "---------------------------------\n";
}
