#pragma once

#include "defines.hxx"
#include "graph.hxx"
#include "aligned_allocator.hxx"
#include "simd_vectors.hxx"
#include "graph_sampling.hxx"
#include "concurrent_queue.hxx"
#include "utils.hxx"

//	LOGGER HEADER 
#include <LoggerCpp/LoggerCpp.h>

enum verbosity_level{none,total,component_wise,only_algo};

#define MATCHING_DEBUG 0
#define LOGTK_DEBUG 0
#define DUAL_DEBUG 0


/*
 *
 */
template<typename SIMDVECTOR>
class handshake_algo_MT
{
    public:
        typedef typename SIMDVECTOR::type T;
        typedef typename SIMDVECTOR::vectorizer vectorizer;

        struct options
        {
            size_t num_iters;
            size_t num_threads;
            size_t primal_computation_frequency;

            options(const size_t nIters=100,const size_t nThreads=1, const size_t primal_freq=10):
                num_iters(nIters), num_threads(nThreads), 
                primal_computation_frequency(primal_freq)
            {

            }
        };

        struct node_info
        {
            int id;				//	id of the node
            int K;				//	
            std::mutex m_lock;	//	Maybe, there should be a mutex here, as well.
            T* data;            //  MAYBE THIS SHOULD BE VECTORIZER SIZE
            T* primal_data;     //  MAYBE THIS SHOULD BE VECTORIZER SIZE
            
            node_info(): id(-1),K(-1),data(nullptr),primal_data(nullptr)
            {

            } 
        };

        struct edge_info
        {
			int id;				//	The many failings of Ramachandra Guha
            int from;			//	
            int to;				//	
            int K1;				//	
            int K2;				//	
            std::mutex m_lock;	//	So, there is a mutex here. Great.
            T* data;            //  MAYBE THIS SHOULD BE VECTORIZER SIZE
            T* primal_data;     //  MAYBE THIS SHOULD BE VECTORIZER SIZE
            
            edge_info(): id(-1),from(-1),to(-1),K1(-1),K2(-1),data(nullptr),primal_data(nullptr)
            {
            
            }            
        };

        /*!
         *  Constructor
         */
        handshake_algo_MT(Graph<T>& graph, const options s_opts);

        /*!
         *  Destructor
         */
        ~handshake_algo_MT();

        void memory_alignment_1(Graph<T>& graph);
        void print_aligned_allocator();
        void wait_finished();
        void do_simd_handshake();
        void infer();
        void compute_ordering();
        void compute_ordering_boost();
        void thread_fn(int thread_id);
        void print_data();
        void set_up_work_queue();
        void copy_potentials_outside(std::vector<std::vector<T>>& unaries, std::vector<std::vector<T>>& pairwise);
        void locked_handshake(const int edge_idx, const int thread_id);
        void update_dual(const int un1_id, const int un2_id, const T un1_val, const T un2_val);
        typename SIMDVECTOR::type compute_dual(bool also_edges=false);
        bool check_minorant_condition();
        void copy_from_dual_to_primal();
        T compute_primal();
        void compute_adjacency_list();

        std::atomic_int m_num_messages_passed;              //  since this is atomic it should be fine
        std::mutex m_dual_mutex;                            //  should be locked when modifying the dual variables, should be unlocked when done.
        std::mutex m_io_mutex;

        //  maybe write a threadInfo struct, it might end up working better
        std::vector<std::thread> m_threads;                 //  has to be launched via a thread pool
        std::deque<int> m_work_queue;
        std::mutex m_queue_mutex;
        std::condition_variable cv_task;                    //  condition variable to signal a task is available. used in conjunction with m_work_queue
        std::condition_variable cv_finished;                //  condition variable to signal the tasks are finished. used in conjunction with m_work_queue
        std::condition_variable cv_start;
        unsigned int busy;                                  //  indicates the number of threads that are busy

		Log::Logger m_logger;


        //  ONE OF THE THREE VARIABLES TO INDICATE THE TERMINATION CONDITION
        std::vector<std::vector<std::pair<int,int>> > m_adj_list;
        std::atomic_bool m_bailout;                         //  Not sure if all three are required
        std::atomic_bool m_finished;                        //  Not sure if all three are required
        bool m_stop;                                        //  Not sure if all three are required
        bool m_start;
        
        std::atomic_uint processed;                         //  not quite sure
        //  Member variables for thread pool
        int m_unary_size;                                   //   
        int m_pw_size;                                      //  
        int m_align_size;                                   //  

        std::vector<node_info*> nodes_info;                 //  array for node info
        std::vector<edge_info*> edges_info;                 //  array for edge info
        mutable aligned_allocator m_aligned_block;          //   
        std::vector<int> m_ordering;                        //  ordering  
        T* m_dual_values;                                   //  still not done
        std::shared_ptr<Graph<T>> m_graph_ptr;              //  graph ptr
        size_t m_block_size;                                //  TODO: REMOVE
        std::vector<int> m_labelling;
        options opts;
};

/*!
 *
 */
template<typename SIMDVECTOR>
handshake_algo_MT<SIMDVECTOR>::handshake_algo_MT(Graph<T>& graph, const options s_opts): 
    opts(s_opts), m_num_messages_passed(0), busy(0), m_logger("main.Tester")
{
    m_graph_ptr=std::make_shared<Graph<T>>(graph);
    memory_alignment_1(graph);
    compute_ordering_boost();
    set_up_work_queue();
    compute_adjacency_list();
    m_align_size=sizeof(typename SIMDVECTOR::vectorizer)/sizeof(typename SIMDVECTOR::type);    
    
	Log::Manager::setDefaultLevel(Log::Log::eNotice);
    
    // Configure the Output objects
    Log::Config::Vector configList;
    Log::Config::addOutput(configList, "OutputConsole");
    Log::Config::addOutput(configList, "OutputFile");
    Log::Config::setOption(configList, "filename",          "log.txt");
    Log::Config::setOption(configList, "filename_old",      "log.old.txt");
    Log::Config::setOption(configList, "max_startup_size",  "0");
    Log::Config::setOption(configList, "max_size",          "10000");
    
};

/*!
 *  Layout: | node_info_core1 | ... | node_info_coreN | edge_info_core1 | ... | edge_info_coreN |
 */ 
template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::memory_alignment_1(Graph<T>& graph)
{

    //  allocating memory for node info
    for(int i=0;i<graph.m_nodes.size();++i)     m_aligned_block.allocate<node_info>();
    for(int e=0;e<graph.m_edges.size();++e)     m_aligned_block.allocate<edge_info>();
    int align_size=sizeof(vectorizer)/sizeof(T);

    m_unary_size=0;
    m_pw_size=0;

    for(int i=0;i<graph.m_data_storage.m_unaries.size();++i)
    {
        int skip_step=RND_UP(graph.m_data_storage.m_unaries[i].size(),align_size);
        m_aligned_block.allocate<T>(static_cast<int>(skip_step));               //  dual unaries
        m_aligned_block.allocate<T>(static_cast<int>(skip_step));               //  primal unaries
    }
 
    for(auto& ed:graph.m_edges)
    {
        int K1=ed.m_d1;
        int K2=ed.m_d2;
        int skip_step=RND_UP(K2,align_size);                                    //  TODO: CHANGE THIS PART
        m_aligned_block.allocate<T>(static_cast<int>(skip_step*K1));            //  dual pairwise
        m_aligned_block.allocate<T>(static_cast<int>(skip_step*K1));            //  primal pairwise
    }

    m_aligned_block.init();
    nodes_info.resize(graph.m_nodes.size());
    edges_info.resize(graph.m_edges.size());

    for(int i=0;i<graph.m_nodes.size();++i)
    {
        Node<T>& nd=graph.m_nodes[i];
        nodes_info[i]=m_aligned_block.allocate<node_info>();
        nodes_info[i]->id=graph.m_nodes[i].m_idx;
        nodes_info[i]->K=graph.m_nodes[i].m_d1;
        nodes_info[i]->data=nullptr;
        nodes_info[i]->primal_data=nullptr;
    }

    for(int e=0;e<graph.m_edges.size();++e)
    {
        Edge<T>& ed=graph.m_edges[e];
        edges_info[e]=m_aligned_block.allocate<edge_info>();
        edges_info[e]->id=graph.m_edges[e].m_idx;
        edges_info[e]->from=graph.m_edges[e].m_from;
        edges_info[e]->to=graph.m_edges[e].m_to;
        edges_info[e]->K1=graph.m_edges[e].m_d1;
        edges_info[e]->K2=graph.m_edges[e].m_d2;
    }

    m_unary_size=0;
    for(int i=0;i<graph.m_nodes.size();++i)
    {
        int skip_step=RND_UP(nodes_info[i]->K,align_size);
        nodes_info[i]->data=m_aligned_block.allocate<T>(static_cast<int>(skip_step));
        nodes_info[i]->primal_data=m_aligned_block.allocate<T>(static_cast<int>(skip_step)); 
        for(int j=0;j<nodes_info[i]->K;++j)
        {
            nodes_info[i]->data[j]=graph.m_data_storage.m_unaries[i][j];
        } 
        m_unary_size+=skip_step*2; 
    }

/*
    for(int i=0;i<graph.m_nodes.size();++i)
    {
        T* data=nodes_info[i]->data;
        std::cout << "Unary " << nodes_info[i]->id << ": ";
        for(int j=0;j<nodes_info[i]->K;++j)
        {
            std::cout << data[j] << " ";
        }
        std::cout << "\n";
    }
*/
    
    m_pw_size=0;
    for(int e=0;e<graph.m_edges.size();++e)
    {
        int skip_step=RND_UP(edges_info[e]->K2,align_size);          //  todo: change this
        edges_info[e]->data=m_aligned_block.allocate<T>(static_cast<int>(skip_step*edges_info[e]->K1));
        edges_info[e]->primal_data=m_aligned_block.allocate<T>(static_cast<int>(skip_step*edges_info[e]->K1));
        for(int j=0;j<edges_info[e]->K1;++j)
        {
            for(int k=0;k<edges_info[e]->K2;++k)
            {
                edges_info[e]->data[j*skip_step+k]=graph.m_data_storage.m_pairwise[e][j*edges_info[e]->K2+k];
            }
        }
        m_pw_size+=skip_step*edges_info[e]->K1*2;
    }

/*
    std::cout << "At edges\n";
    for(int e=0;e<graph.m_edges.size();++e)
    {
        int skip_step=RND_UP(edges_info[e]->K2,4);          //  todo: change this
        std::cout << graph.m_edge_indices[e].first << " -> " << graph.m_edge_indices[e].second << "\n";
        T* ptr=edges_info[e]->data;
        for(int j=0;j<edges_info[e]->K1;++j)
        {
            for(int k=0;k<edges_info[e]->K2;++k)
            {
                std::cout << ptr[j*skip_step+k] << " ";
            }
            std::cout << "\n";
        }
        std::cout << "\n";
    }
*/

    m_dual_values=(T*)malloc(sizeof(T)*(graph.m_nodes.size()+graph.m_edges.size()));
    for(int i=0;i<graph.m_nodes.size()+graph.m_edges.size();++i) m_dual_values[i]=0;
}


template<typename SIMDVECTOR>
handshake_algo_MT<SIMDVECTOR>::~handshake_algo_MT()
{
    //  All the pointers have to be deallocated
    for(int i=0;i<nodes_info.size();++i)
    {
        nodes_info[i]->data=nullptr;
        nodes_info[i]->primal_data=nullptr;
    }
    
    for(int e=0;e<edges_info.size();++e)
    {
        edges_info[e]->data=nullptr;
        edges_info[e]->primal_data=nullptr;
    }
}

template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::compute_adjacency_list()
{
    m_adj_list.resize(m_graph_ptr->m_nodes.size()); 
    for(int e=0;e<m_graph_ptr->m_edges.size();++e)
    {
        int from=m_graph_ptr->m_edges[e].m_from;
        int to=m_graph_ptr->m_edges[e].m_to;
        std::pair<int,int> pr_from(from,e);
        std::pair<int,int> pr_to(to,e);
        m_adj_list[from].push_back(pr_to);
        m_adj_list[to].push_back(pr_from);
    }
}



template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::print_aligned_allocator()
{
    T* ptr=nodes_info[0]->data;
    for(int i=0;i<m_unary_size+m_pw_size;++i)
    {
        std::cout << ptr[i] << " ";
    }
    std::cout << "\n";
}

template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::compute_ordering()
{
    m_graph_ptr->print();
    m_ordering=sample_graph_1(*m_graph_ptr);
}

template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::compute_ordering_boost()
{
    std::vector<bool> m_edges_done(m_graph_ptr->m_edges.size(),false);
    int num_edges_remaining=m_graph_ptr->m_edges.size();
    std::map<int,int> nd2edgeID;
    
    for(int e=0;e<m_graph_ptr->m_edge_indices.size();++e)
    {
        int from=m_graph_ptr->m_edge_indices[e].first;
        int to=m_graph_ptr->m_edge_indices[e].second;
        int eHASH=get_hash_code(from,to,m_graph_ptr->m_nodes.size());
        nd2edgeID[eHASH]=e;
    }

    typedef boost::adjacency_list<boost::vecS,boost::vecS,boost::undirectedS> bGraph;
    bGraph gr(m_graph_ptr->m_nodes.size());

    for(int e=0;e<m_graph_ptr->m_edge_indices.size();++e)
    {
        int from=m_graph_ptr->m_edge_indices[e].first;
        int to=m_graph_ptr->m_edge_indices[e].second;
        boost::add_edge(from,to,gr);
    }

    m_ordering.resize(m_graph_ptr->m_edges.size());

    int current_edge=0;
    while(num_edges_remaining>0)
    {
#if MATCHING_DEBUG        
        std::cout << "iterate directly over edges\n";
        auto es = boost::edges(gr);
        for (auto eit = es.first; eit != es.second; ++eit) 
        {
            std::cout << boost::source(*eit, gr) << ' ' << boost::target(*eit, gr) << std::endl;
        }
        std::cout << "\n\n";
#endif

        std::vector<boost::graph_traits<bGraph>::vertex_descriptor> matching(m_graph_ptr->m_nodes.size());
        bool success=boost::checked_edmonds_maximum_cardinality_matching(gr,&matching[0]);
        assert(success);

        boost::graph_traits<bGraph>::vertex_iterator vi,vi_end;

#if MATCHING_DEBUG
        std::cout << "Matching Selected\n";
        for(boost::tie(vi,vi_end) = boost::vertices(gr); vi!=vi_end; ++vi)
        {
            if (matching[*vi] != boost::graph_traits<bGraph>::null_vertex() && *vi < matching[*vi])
            {
                std::cout << *vi << " " << matching[*vi] << "\n";
            }
        }
#endif
        for(boost::tie(vi,vi_end) = boost::vertices(gr); vi != vi_end; ++vi)
        {
            if (matching[*vi] != boost::graph_traits<bGraph>::null_vertex() && *vi < matching[*vi])
            {
                int from,to;
                if((*vi)<matching[*vi])
                {
                    from=*vi;
                    to=matching[*vi];
                }
                else
                {
                    to=*vi;
                    from=matching[*vi];
                }
                int hashCode=get_hash_code(from,to,m_graph_ptr->m_nodes.size());
                int eID=nd2edgeID[hashCode];
                m_edges_done[eID]=true;
                num_edges_remaining--;
                boost::remove_edge(from,to,gr);     //  remove the edge from the graph
                m_ordering[current_edge]=eID;
                current_edge++;
            }
        }
    }
}

template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::infer()
{
    T m_dual=compute_dual(true);
	m_logger.debug() << "Initial dual: " << m_dual << "\n";


	/*
		Launch threads. Place them in the m_threads vector
	*/
    for(unsigned int i=0;i<opts.num_threads;++i)
    {
        std::cout << "at i=" << i << "\n";
        m_threads.emplace_back(std::bind(&handshake_algo_MT::thread_fn,this,std::ref(i)));
    }

	/*!
		Not really required, these four lines.
		Threads wait on cv_start to signal that it is OK to start
	*/
    std::unique_lock<std::mutex> latch1(m_queue_mutex);         //  lock the queue mutex. not sure why
    m_start=true;                                               //  indicate m_start=true
    cv_start.notify_all();                                      //  notify all the threads.
    latch1.unlock();                                            //  unlock the latch

	/*
		Wait until all the threads are finished	
	*/
    wait_finished();            //  It will not proceed further than this.

	/*
		Signal that all the threads are stopped
	*/
    std::unique_lock<std::mutex> latch(m_queue_mutex);          //  latch the queue mtuex
    m_stop=true;                                                //  indicate that the thing has stopped
    cv_task.notify_all();                                       //  notify all the threads in some way. What does this do?
    latch.unlock();                                             //  unlock the latch

    for(auto& t: m_threads)
        t.join();

    m_dual=compute_dual();

	m_logger.debug() <<"Final Dual: " << m_dual << "\n";

    T m_primal=compute_primal();
    std::cout << "Final Dual: " << m_dual << " Primal: " << m_primal << "\n";
}

template<typename SIMDVECTOR>
bool handshake_algo_MT<SIMDVECTOR>::check_minorant_condition()
{
    bool flag=true;

    T EPSILON=1e-6;

    for(int e=0;e<edges_info.size();++e)
    {
        T* data=edges_info[e]->data;
        for(int j=0;j<edges_info[e]->K1;++j)
        {
            T min_val=std::numeric_limits<T>::infinity();
            for(int k=0;k<edges_info[e]->K2;++k)
            {
                if(min_val>data[j*edges_info[e]->K2+k])
                {
                    min_val=data[j*edges_info[e]->K2+k];
                }
            }
            if(fabs(min_val)>EPSILON)
            {
                flag=false;
            }
        }

        for(int k=0;k<edges_info[e]->K2;++k)
        {
            T min_val=std::numeric_limits<T>::infinity();
            for(int j=0;j<edges_info[e]->K1;++j)
            {
                if(min_val>data[j*edges_info[e]->K2+k])
                {
                    min_val=data[j*edges_info[e]->K2+k];
                }               
            }
            if(fabs(min_val)>EPSILON)
            {
                flag=false;
            }
        }
    }
    return flag;
}


template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::print_data()
{
    print_dashed_line();
    T* ptr;
    for(int i=0;i<nodes_info.size();++i)
    {
        ptr=nodes_info[i]->data;
        std::cout << "At unary: " << nodes_info[i]->id << "\n";
        for(int j=0;j<nodes_info[i]->K;++j)
        {
            std::cout << ptr[j] << " ";
        }
        std::cout << "\n";
    }

    for(int e=0;e<edges_info.size();++e)
    {
        std::cout << "from: " << edges_info[e]->from << " to: " << edges_info[e]->to << "\n";
        std::cout << "K1: " << edges_info[e]->K1 << " K2: " << edges_info[e]->K2 << "\n";
        ptr=edges_info[e]->data;
        for(int j=0;j<edges_info[e]->K1;++j)
        {
            for(int k=0;k<edges_info[e]->K2;++k)
            {
                std::cout << ptr[j*edges_info[e]->K2+k] << " "; 
            }
            std::cout << "\n";
        }
        std::cout << "\n";
    }
    print_dashed_line();
}

template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::thread_fn(const int thread_id)
{
    std::unique_lock<std::mutex> latch1(m_queue_mutex);
    cv_start.wait(latch1,[this](){return m_start;});
    cv_start.notify_all();                                              //  everyone is allowed to start
    latch1.unlock();

    while(true)          //  This is an atomic variable
    {
        std::unique_lock<std::mutex> latch(m_queue_mutex);              //    
        cv_task.wait(latch,[this](){return m_stop|| !m_work_queue.empty();});    //  if stop is true || tasks is not empty return true. 
        if(!m_work_queue.empty())                                              //  thread now has control of the mutex
        {
            ++busy;                                                     //  got busy 

            int edge_idx=m_work_queue.front();                          //  extract the element from the work queue
            m_work_queue.pop_front();                                   //  pop element from the front of the queue

            latch.unlock();                                             //  unlock the mutex again

            locked_handshake(edge_idx, thread_id);                      //  perform the locked handshake

           T m_dual=compute_dual();                                    //  compute_dual
			
            latch.lock();                                               //  lock the latch
            --busy;                                                     //  work is done
            cv_finished.notify_one();                                   //  notify the condition variable
        }
        else if(m_stop)
        {
            break;
        }
    }
}

template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::wait_finished()
{
    std::unique_lock<std::mutex> lock(m_queue_mutex);
    m_io_mutex.lock();
    std::cout << "wait finished\n";
    m_io_mutex.unlock();
    cv_finished.wait(lock,[this](){ return m_work_queue.empty() &&(busy==0);});  //  thread acquires lock, checks for conditions, releases the lock 
}

template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::copy_potentials_outside(std::vector<std::vector<T>>& unaries, std::vector<std::vector<T>>& pairwise)
{
    for(int i=0;i<nodes_info.size();++i)
    {
        node_info* nd=nodes_info[i];
        T* ptr=nd->data;
        std::vector<T> unary(nd->K);
        for(int j=0;j<nd->K;++j)    unary[j]=ptr[j];
        unaries.push_back(unary);
    }

    for(int e=0;e<edges_info.size();++e)
    {
        edge_info* ed=edges_info[e];
        T* ptr=ed->data;
        std::vector<T> pw(ed->K1*ed->K2);
        for(int j=0;j<ed->K1*ed->K2;++j)    pw[j]=ptr[j];
        pairwise.push_back(pw);
    }
}



template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::set_up_work_queue()
{
    //  work queue
    std::vector<int> m_ordering_full;
    for(int i=0;i<opts.num_iters;++i)
    {
        m_ordering_full.insert(m_ordering_full.end(),m_ordering.begin(),m_ordering.end());
    }
    m_work_queue.insert(m_work_queue.end(),m_ordering_full.begin(),m_ordering_full.end());
}


template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::locked_handshake(const int edge_idx, const int thread_id)
{
    int from=edges_info[edge_idx]->from;
    int to=edges_info[edge_idx]->to;
    int K1=edges_info[edge_idx]->K1;
    int K2=edges_info[edge_idx]->K2;

    T* un1_ptr=nodes_info[from]->data;
    T* un2_ptr=nodes_info[to]->data;
    T* pw_ptr=edges_info[edge_idx]->data;

    T proportion;

    //  lock the edge, from node, to node
    std::unique_lock<std::mutex> lock_from(nodes_info[from]->m_lock,std::defer_lock);
    std::unique_lock<std::mutex> lock_to(nodes_info[to]->m_lock,std::defer_lock);

    //  simultaneously lock all nodes
    std::lock(lock_from,lock_to);		// simultaneously lock the from and to node. std::lock -> avoids deadlock

    //  push from left and right
    for(auto i=0;i<K1;++i)
    {
        for(auto j=0;j<K2;++j)
        {
            pw_ptr[i*K2+j]+=un1_ptr[i]+un2_ptr[j];
        }
    }

    //  can this be sped up
    proportion=0.5;
    //  pull half from left
    for(auto i=0;i<K1;++i)
    {
        T min_val=pw_ptr[i*K2+0];
        for(auto j=1;j<K2;++j)
        {
            if(pw_ptr[i*K2+j]<min_val)  min_val=pw_ptr[i*K2+j];
        }
        un1_ptr[i]=proportion*min_val;
        for(auto j=0;j<K2;++j)  pw_ptr[i*K2+j]-=(proportion)*min_val;
    }

    //  pull from right
    T un2_min_val=std::numeric_limits<T>::infinity();
    for(auto j=0;j<K2;++j)
    {
        T min_val=pw_ptr[0*K2+j];
        for(auto i=1;i<K1;++i)
        {
            if(pw_ptr[i*K2+j]<min_val)  min_val=pw_ptr[i*K2+j];
        }
        un2_ptr[j]=min_val;
        if(un2_min_val>un2_ptr[j])
        {
            un2_min_val=un2_ptr[j];
        }
        for(auto i=0;i<K1;++i)      pw_ptr[i*K2+j]-=min_val;
    }

    //  can this be sped up
    //  pull from left
    T un1_min_val=std::numeric_limits<T>::infinity();
    for(auto i=0;i<K1;++i)
    {
        T min_val=pw_ptr[i*K2+0];
        for(auto j=1;j<K2;++j)
        {
            if(pw_ptr[i*K2+j]<min_val)  min_val=pw_ptr[i*K2+j];
        }
        un1_ptr[i]+=min_val;
        if(un1_min_val>un1_ptr[i])
        {   
            un1_min_val=un1_ptr[i];         //  compute the min value of un1
        }
        for(auto j=0;j<K2;++j)  pw_ptr[i*K2+j]-=min_val;
    }

    update_dual(from,to,un1_min_val,un2_min_val);           //  

    T m_dual=compute_dual();
    m_num_messages_passed++;
	
	m_logger.debug() << "num_messages: " << m_num_messages_passed << " dual: " << m_dual <<"\n";
}

template<typename SIMDVECTOR>
void handshake_algo_MT<SIMDVECTOR>::update_dual(const int un1_id, const int un2_id, const T un1_val, const T un2_val)
{
    std::unique_lock<std::mutex> lck(m_dual_mutex);
    m_dual_values[un1_id]=+un1_val;
    m_dual_values[un2_id]=+un2_val;
}


template<typename SIMDVECTOR>
typename SIMDVECTOR::type handshake_algo_MT<SIMDVECTOR>::compute_dual(bool also_edges)
{
    T dual_val=0;
	
	std::unique_lock<std::mutex> lck(m_dual_mutex);
	
	for(int i=0;i<nodes_info.size();++i)
    {
		T* data=nodes_info[i]->data;
		T min_val=data[0];
		for(int j=1;j<nodes_info[i]->K;++j) if(min_val>data[j]) min_val=data[j];
		dual_val+=min_val;
    }


    if(also_edges)
    {
        for(int e=0;e<edges_info.size();++e)
        {
            T* data=edges_info[e]->data;
            T min_val=data[0];
            
            for(int j=1;j<((edges_info[e]->K1)*(edges_info[e]->K2));++j)
            {
                if(min_val>data[j]) min_val=data[j];
            } 
            dual_val+=min_val;
        }
        for(int i=0;i<nodes_info.size()+edges_info.size();++i) dual_val+=m_dual_values[i]; 
    }
    return dual_val;
}




template<typename SIMDVECTOR>
void  handshake_algo_MT<SIMDVECTOR>::copy_from_dual_to_primal()
{
    int align_size=sizeof(vectorizer)/sizeof(T);

    for(int i=0;i<nodes_info.size();++i)
    {
        node_info* nd=nodes_info[i];
        T* ptr=nd->data;
        T* primal_ptr=nd->primal_data;
        int K=nd->K;
        int skip_step=RND_UP(K,align_size);
        memcpy(primal_ptr,ptr,sizeof(T)*(skip_step));
    }

    for(int e=0;e<edges_info.size();++e)
    {
        edge_info* ed=edges_info[e];
        T* ptr=ed->data;
        T* primal_ptr=ed->primal_data;
        int K1=ed->K1;
        int K2=ed->K2;
        int skip_step=RND_UP(K2,align_size);                                    //  TODO: CHANGE THIS PARTi
        memcpy(primal_ptr,ptr,sizeof(T)*(K1*skip_step));
    }
}

template<typename SIMDVECTOR>
typename SIMDVECTOR::type handshake_algo_MT<SIMDVECTOR>::compute_primal()
{
    copy_from_dual_to_primal();
    std::vector<bool> m_visited(nodes_info.size(),false);
    std::deque<int> q;
    m_visited[0]=true;
    q.push_back(0);
    m_labelling.resize(m_graph_ptr->m_nodes.size(),-1);

    int align_size=sizeof(vectorizer)/sizeof(T);

    std::vector<std::pair<int,int>>::iterator i;
    while(!q.empty())
    {
        int s=q.front();
        q.pop_front();

        T* ptr=nodes_info[s]->primal_data;

        m_labelling[s]=compute_min_array_location(ptr,nodes_info[s]->K);
        for(i=m_adj_list[s].begin();i!=m_adj_list[s].end();++i)
        {
            int node2=(*i).first;
            int edge=(*i).second;

            T* e_ptr=edges_info[edge]->primal_data;
            T* nd2_ptr=nodes_info[node2]->primal_data;

            if(s<node2)
            {
                int Kk=nodes_info[node2]->K;
                for(int j=0;j<Kk;++j)
                {
                    nd2_ptr[j]+=e_ptr[m_labelling[s]*RND_UP(Kk,align_size)+j]; 
                }
            }
            else
            {
                int Kk=nodes_info[node2]->K;
                for(int j=0;j<Kk;++j)
                {
                    nd2_ptr[j]+=e_ptr[j*RND_UP(Kk,align_size)+m_labelling[s]];
                }
            }

            if(!m_visited[node2])
            {
                m_visited[node2]=true;

                q.push_back(node2);
            }
        }
    }

    T primal=0.0;
    for(int i=0;i<nodes_info.size();++i)
    {
        primal+=nodes_info[i]->data[m_labelling[i]];
    }

    for(int e=0;e<edges_info.size();++e)
    {
        int from = edges_info[e]->from;
        int to   = edges_info[e]->to;
        int Kk=RND_UP(edges_info[e]->K2,align_size);
        primal+=edges_info[e]->data[m_labelling[from]*Kk+m_labelling[to]];
    }
    return primal;
}

