#pragma once

#include "defines.hxx"

/*
 * Does a node need to point to neighbors? ? ?
 *  */

template<typename T>
class Node
{
public:
    int m_d1;
    int m_idx;
    T* m_data;
    T* m_primal;
    std::vector<int> m_nbr_edges;

    std::mutex m_mutex;

    Node():m_data(nullptr),m_primal(nullptr)
    {

    }

    //
    Node(const int IDX, const int D1):
    m_d1(D1),m_idx(IDX),m_data(nullptr),m_primal(nullptr)
    {

    }

    //  copy constructor
    //  not technically correct as a copy constructor but nobody gives a fuck
    Node(const Node<T>& other)
    {
        m_data=other.m_data;
        m_primal=other.m_primal;
        m_idx=other.m_idx;
        m_d1=other.m_d1;
        m_nbr_edges=other.m_nbr_edges;
        //m_nbr_edges=other.m_nbr_edges;
    }

    //  move constructor
    Node(Node<T>&& other)
    {
        m_data=std::move(other.m_data);
        m_primal=std::move(other.m_primal);
        other.m_data=nullptr;
        m_idx=other.m_idx;
        m_d1=other.m_d1;
        //m_nbr_edges=other.m_nbr_edges;
    }

    //  copy assignment operator
    Node<T>& operator=(const Node<T>& other)
    {
        Node<T> tmp(other);
        *this=std::move(tmp);
        return *this;
    }

    //  move assignment operator
    Node<T>& operator=(Node<T>&& other)
    {
        delete[] m_data;
        m_data=other.m_data;
        other.m_data=nullptr;
        //m_nbr_edges=other.m_nbr_edges;
        m_d1=other.m_d1;
        m_idx=other.m_idx;
        return *this;
    }


    void print()
    {
        std::cout << "NODE INFO: ";
        std::cout << "idx: " << m_idx << " ";
        std::cout << "m_d1: " << m_d1 << "\n";
/*
        if(m_nbr_edges.size()>0)
        {
            std::cout << "neighboring edges: ";
            for(auto& e :m_nbr_edges)
            {
                std::cout << e << " ";
            }
            std::cout << "\n";
        }
        else
        {
            std::cout << "Uninitialized neighbor list or isolated node\n";
        }

        if(m_data==nullptr)
        {
            std::cout << "m_data is nullptr\n";
        }

        if(m_primal==nullptr)
        {
            std::cout << "m_primal is nullptr\n";
        }
*/
    }

    ~Node()
    {
        m_data=nullptr;
    }

};



template<typename T>
class dNode
{
public:

    int m_d1;
    int m_idx;
    T* m_data;
    T* m_primal;

    std::vector<int> m_incoming_edges;
    std::vector<int> m_outgoing_edges;

    std::mutex m_mutex;

    dNode():m_data(nullptr),m_primal(nullptr)
    {
//        std::cout << "NODE 0\n";
    }

    dNode(const int IDX, const int D1):
            m_d1(D1),m_idx(IDX),m_data(nullptr),m_primal(nullptr)
    {
//        std::cout << "NODE 1\n";
    }

    //  copy constructor
    //  not technically correct as a copy constructor but nobody gives a fuck
    dNode(const dNode<T>& other)
    {
//        std::cout << "NODE 2\n";
        m_data=other.m_data;
        m_primal=other.m_primal;
        m_idx=other.m_idx;
        m_d1=other.m_d1;
        m_incoming_edges=other.m_incoming_edges;
        m_outgoing_edges=other.m_outgoing_edges;
        //m_nbr_edges=other.m_nbr_edges;
    }

    //  move constructor
    dNode(dNode<T>&& other)
    {
//        std::cout << "NODE 3\n";
        m_data=std::move(other.m_data);
        m_primal=std::move(other.m_primal);
        other.m_data=nullptr;
        m_idx=other.m_idx;
        m_d1=other.m_d1;
        m_incoming_edges=other.m_incoming_edges;
        m_outgoing_edges=other.m_outgoing_edges;
    }

    //  copy assignment operator
    dNode<T>& operator=(const dNode<T>& other)
    {
//        std::cout << " = OPERATOR FOR NODE\n";
        Node<T> tmp(other);
        *this=std::move(tmp);
        return *this;
    }

    //  move assignment operator
    dNode<T>& operator=(dNode<T>&& other)
    {
        delete[] m_data;
        m_data=other.m_data;
        other.m_data=nullptr;
        m_incoming_edges=other.m_incoming_edges;
        m_outgoing_edges=other.m_outgoing_edges;
        m_d1=other.m_d1;
        m_idx=other.m_idx;
        return *this;
    }


    void print()
    {
        std::cout << "NODE INFO: ";
        std::cout << "idx: " << m_idx << " ";
        std::cout << "m_d1: " << m_d1 << "\n";

        std::cout << "incoming edges: ";
        if(m_incoming_edges.size()>0)
        {

            for(auto& e :m_incoming_edges)
            {
                std::cout << e << " ";
            }
            std::cout << "\n";
        }
        else
        {
            std::cout << "NO INCOMING EDGES\n";
        }

        std::cout << "outgoing edges: ";
        if(m_outgoing_edges.size()>0)
        {
            for(auto& e :m_outgoing_edges)
            {
                std::cout << e << " ";
            }
            std::cout << "\n";
        }
        else
        {
            std::cout << "NO OUTGOING EDGES\n";
        }

        if(m_data==nullptr)
        {
            std::cout << "m_data is nullptr\n";
        }

        if(m_primal==nullptr)
        {
            std::cout << "m_primal is nullptr\n";
        }

    }

    ~dNode()
    {
        m_data=nullptr;
    }

};
