#pragma once

#include "defines.hxx"
#include "utils.hxx"
#include "graph.hxx"
#include "edge.hxx"
#include "node.hxx"

template<typename T>
Graph<T> read_graph(const std::string& filename)
{
    Graph<T> graph;
    std::ifstream myfile(filename.c_str());
    if(myfile.is_open())
    {
        int nV, nF,nE;
        int factor_type;
        int id1, id2, sz1, sz2;

        myfile >> nV >> nF;
        nE=nF-nV;
 
        std::cout << "nV: " << nV <<  " nE: " << nE << "\n";

        graph.m_nV=nV;
        graph.m_nE=nE;
        graph.m_nodes.resize(nV);
        graph.m_edges.resize(nE);
        graph.m_edge_indices.resize(nE);
        graph.m_data_storage.m_unaries.resize(nV);
        graph.m_data_storage.m_pairwise.resize(nE);

        int i=0;
        int eID=0;
        while(i<nF)
        {
            myfile >> factor_type; 
            if(factor_type==1)
            {
                myfile >> id1 >> sz1;
                std::vector<T> unary(sz1);
                for(int ii=0;ii<sz1;++ii)   myfile >> unary[ii]; 
                Node<T> nd(id1,sz1);
                graph.m_nodes[id1]=nd;
                graph.m_data_storage.m_unaries[id1]=unary;
            }
            else if(factor_type==2)
            {
                myfile >> id1 >> id2 >> sz1 >> sz2;
                std::pair<int,int> pr(id1,id2);
                graph.m_edge_indices[eID]=pr;
                std::vector<T> pairwise(sz1*sz2);
                for(int ii=0;ii<sz1*sz2;++ii)   myfile >> pairwise[ii];
                Edge<T> ed(id1,id2,eID,sz1,sz2);
                graph.m_edges[eID]=ed;
                graph.m_data_storage.m_pairwise[eID]=pairwise;
                ++eID;
            }
            else
            {
                throw std::runtime_error("Unable to figure out factor type\n");
            }

            ++i;
        }
        myfile.close();        
    }
    else
    {
        throw std::runtime_error("Unable to open a file\n");
    }
    return graph;
}


template<typename T>
Graph<T> read_graph2(const std::string& filename)
{
    Graph<T> graph;
    std::ifstream myfile(filename.c_str());
    if(myfile.is_open())
    {
        int nV, nF,nE;
        int factor_type;
        int id1, id2, sz1, sz2;

        myfile >> nV >> nE;
        nF=nE+nV;

        graph.m_nV=nV;
        graph.m_nE=nE;
        graph.m_nodes.resize(nV);
        graph.m_edges.resize(nE);
        graph.m_edge_indices.resize(nE);
        graph.m_data_storage.m_unaries.resize(nV);
        graph.m_data_storage.m_pairwise.resize(nE);

        int i=0;
        int eID=0;
        while(i<nF)
        {
            myfile >> factor_type; 
            std::cout << factor_type << "\n";
            if(factor_type==1)
            {
                myfile >> id1 >> sz1;
                std::vector<T> unary(sz1);
                for(int ii=0;ii<sz1;++ii)   myfile >> unary[ii];
                Node<T> nd(id1,sz1);
                graph.m_nodes[id1]=nd;
                graph.m_data_storage.m_unaries[id1]=unary;
            }
            else if(factor_type==2)
            {
                myfile >> id1 >> id2 >> sz1 >> sz2;
                std::pair<int,int> pr(id1,id2);
                graph.m_edge_indices[eID]=pr;
                std::vector<T> pairwise(sz1*sz2);
                for(int ii=0;ii<sz1*sz2;++ii)   myfile >> pairwise[ii];
                Edge<T> ed(id1,id2,eID,sz1,sz2);
                graph.m_edges[eID]=ed;
                graph.m_data_storage.m_pairwise[++eID]=pairwise;
            }
            else
            {
                throw std::runtime_error("Unable to figure out factor type\n");
            }

            ++i;
        }
        myfile.close();        
    }
    else
    {
        throw std::runtime_error("Unable to open a file\n");
    }
    return graph;
}

